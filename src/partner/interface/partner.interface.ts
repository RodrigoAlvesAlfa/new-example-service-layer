export class Partner {
  CardCode: string;
  CardName: string;
  CardType?: string;
  Phone1: string;
  Phone2: string;
  BPAddresses?: [
    {
      AddressName?: string;
      Street: string;
      Block: string;
      ZipCode: string;
      City: string;
      County: string;
      Country?: string;
      State: string;
      BuildingFloorRoom?: string;
      AddressType?: string;
      TypeOfAddress?: string;
      StreetNo?: string;
      BPCode?: string;
    },
  ];
}
