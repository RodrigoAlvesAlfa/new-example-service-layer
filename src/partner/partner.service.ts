import { Injectable } from '@nestjs/common';
import { LoginService } from '../login/login.service';
import { Partner } from './interface/partner.interface';
import { HttpService } from '../http/http.service';

@Injectable()
export class PartnerService {
  constructor(
    private readonly loginService: LoginService,
    private readonly httpService: HttpService,
  ) {}

  async createPartner(data: Partner) {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };
    let count = 0;
    for (const address of data.BPAddresses) {
      count = count + 1;
      address.AddressName = `Entrega${count}`;
      address.AddressType = 'bo_ShipTo';
      address.Country = 'BR';
    }

    const response = await this.httpService.post(
      'BusinessPartners',
      data,
      config,
    );
    await this.loginService.logout(login.data.SessionId);
    if (response.data) {
      return {
        error: null,
        message: 'Create Partner Success',
        code: response.data.ItemCode,
      };
    } else {
      return {
        error: response.error.innerMessage,
        message: 'Error to create Partner',
        code: data.CardCode,
      };
    }
  }

  async allPartners() {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };
    const response = await this.httpService.get('BusinessPartners', config);

    await this.loginService.logout(login.data.SessionId);
    return response.data;
  }

  async updatePartner(data: Partner) {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };
    const partner: any = await this.getPartner(data.CardCode);
    if (partner.error) {
      return partner;
    }
    const response = await this.httpService.patch(
      `BusinessPartners('${data.CardCode}')`,
      data,
      config,
    );
    await this.loginService.logout(login.data.SessionId);
    return response.data;
  }

  async getPartner(id: string) {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };
    const response = await this.httpService.get(
      `BusinessPartners('${id}')`,
      config,
    );
    await this.loginService.logout(login.data.SessionId);
    if (!response.data) {
      return {
        error: response.error.innerMessage,
        message: 'Partner not found',
        code: id,
      };
    }
    return response.data;
  }

  async deletePartner(id: string) {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };

    const partner: any = await this.getPartner(id);
    if (partner.error) {
      return partner;
    }

    const response = await this.httpService.delete(
      `BusinessPartners('${id}')`,
      config,
    );
    await this.loginService.logout(login.data.SessionId);
    if (response.error.code == 204) {
      return {
        error: null,
        message: 'Item excluded',
        code: id,
      };
    } else {
      return {
        error: response.error.innerMessage,
        message: 'Partner not excluded',
        code: id,
      };
    }
  }
}
