import {
  Controller,
  Post,
  Body,
  Get,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { Partner } from './interface/partner.interface';
import { PartnerService } from './partner.service';

@Controller('partner')
export class PartnerController {
  constructor(readonly partnerService: PartnerService) {}
  @Post()
  async createPartner(@Body() body: Partner) {
    return this.partnerService.createPartner(body);
  }

  @Get()
  async allPartner() {
    return this.partnerService.allPartners();
  }

  @Patch()
  async updatePartner(@Body() body: Partner) {
    return this.partnerService.updatePartner(body);
  }

  @Get(':id')
  async getPartnerById(@Param() params) {
    return this.partnerService.getPartner(params.id);
  }

  @Delete(':id')
  async deletePartnerById(@Param() params) {
    return this.partnerService.deletePartner(params.id);
  }
}
