import { Module } from '@nestjs/common';
import { PartnerService } from './partner.service';
import { LoginModule } from '../login/login.module';
import { PartnerController } from './partner.controller';
import { HttpModule } from '../http/http.module';

@Module({
  imports: [LoginModule, HttpModule],
  providers: [PartnerService],
  controllers: [PartnerController],
  exports: [PartnerService],
})
export class PartnerModule {}
