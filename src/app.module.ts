import { Module } from '@nestjs/common';
import { PartnerModule } from './partner/partner.module';
import { ItemModule } from './item/item.module';
import { OrderModule } from './order/order.module';
import { HttpModule } from './http/http.module';

@Module({
  imports: [PartnerModule, ItemModule, OrderModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
