import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { Order } from './interface/order.interface';
import { OrderService } from './order.service';
@Controller('order')
export class OrderController {
  constructor(readonly orderService: OrderService) {}

  @Post()
  async createOrder(@Body() body: Order) {
    return this.orderService.createOrder(body);
  }

  @Get()
  async getAllOrder() {
    return this.orderService.getAllOrder();
  }

  @Get(':id')
  async getOrderById(@Param() params) {
    return this.orderService.getOrder(params.id);
  }
}
