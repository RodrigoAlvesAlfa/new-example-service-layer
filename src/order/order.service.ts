import { LoginService } from '../login/login.service';
import { PartnerService } from '../partner/partner.service';
import { Injectable } from '@nestjs/common';
import { Order, OrderModel, ProductModel } from './interface/order.interface';
import { ItemService } from '../item/item.service';
import { HttpService } from '../http/http.service';
@Injectable()
export class OrderService {
  constructor(
    private readonly loginService: LoginService,
    private readonly partnerService: PartnerService,
    private readonly itemService: ItemService,
    private readonly httpService: HttpService,
  ) {}
  async createOrder(data: Order) {
    const parterResponse = await this.partnerService.getPartner(
      data.Customer.CardCode,
    );
    if (parterResponse.error) {
      const dataRespose = await this.partnerService.createPartner(
        data.Customer,
      );
    }
    for (const item of data.Products) {
      const itemResponse = await this.itemService.getItem(item.Code);
      if (itemResponse.error) {
        throw {
          error: {
            code: 'Item1',
            message: `Item not found ${item.Code}`,
          },
        };
      }
    }

    const Order: OrderModel = {
      CardCode: data.Customer.CardCode,
      Comments: data.Comments,
      BPL_IDAssignedToInvoice: 1,
      DiscountPercent: data.DiscountPercent,
      DocDueDate: data.DueDate,
      DocumentLines: data.Products.map((item) => {
        const items: ProductModel = {
          ItemCode: item.Code,
          Quantity: item.Quantity,
          Price: item.Price,
          DiscountPercent: item.DiscountPercent,
        };
        return items;
      }),
    };

    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };
    const response = await this.httpService.post('/Orders', Order, config);
    await this.loginService.logout(login.data.SessionId);
    if (response.data) {
      return {
        error: null,
        message: 'success',
        numDocument: response.data.DocEntry,
      };
    } else {
      return {
        error: response.error.innerMessage,
        message: 'Error to create Order',
        code: data.Reference,
      };
    }
  }

  async getOrder(id: string) {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };
    const response = await this.httpService.get(`/Orders(${id})`, config);
    await this.loginService.logout(login.data.SessionId);
    return response.data;
  }

  async getAllOrder() {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };
    const response = await this.httpService.get(`/Orders`, config);
    await this.loginService.logout(login.data.SessionId);
    return response.data;
  }
}
