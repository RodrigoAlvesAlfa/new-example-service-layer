import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { LoginModule } from '../login/login.module';
import { OrderController } from './order.controller';
import { PartnerModule } from '../partner/partner.module';
import { ItemModule } from '../item/item.module';
import { HttpModule } from '../http/http.module';

@Module({
  imports: [LoginModule, PartnerModule, ItemModule, HttpModule],
  providers: [OrderService],
  controllers: [OrderController],
})
export class OrderModule {}
