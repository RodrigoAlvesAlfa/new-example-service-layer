import { Partner } from '../../partner/interface/partner.interface';

export class Order {
  DueDate: string;
  Comments: string;
  Reference?: string;
  DiscountPercent: number;
  Products: Product[];
  Customer: Partner;
}

interface Product {
  Code: string;
  Description?: string;
  Quantity: number;
  DiscountPercent: number;
  Price: number;
}

export class OrderModel {
  CardCode: string;
  NumAtCard?: string;
  DocDueDate: string;
  Comments?: string;
  BPL_IDAssignedToInvoice: number;
  OpeningRemarks?: string;
  ClosingRemarks?: string;
  DiscountPercent?: number;
  DocumentLines: ProductModel[];
}

export class ProductModel {
  ItemCode: string;
  ItemDescription?: string;
  Quantity: number;
  Price: number;
  DiscountPercent?: number;
}
