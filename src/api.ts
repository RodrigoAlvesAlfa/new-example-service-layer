import axios from 'axios';
import * as https from 'https';

export const api = axios.create({
  baseURL: 'https://177.85.35.34:50000/b1s/v1',
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
});
