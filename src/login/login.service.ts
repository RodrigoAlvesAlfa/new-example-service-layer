import { Injectable } from '@nestjs/common';
import { HttpService } from '../http/http.service';
@Injectable()
export class LoginService {
  constructor(private readonly httpService: HttpService) {}

  async login() {
    const dataRequest = {
      CompanyDB: 'SBO_MATRIX_PRD',
      Password: 'B1admin@',
      UserName: 'manager',
    };

    const config = { headers: { 'Content-Type': 'application/json' } };

    return this.httpService.post('Login', dataRequest, config).then((r) => {
      if (r.error && r.error.innerMessage == 'Unauthorized') {
        r.error.message = 'Acesso não autorizado.';
      }
      return r;
    });
  }

  async logout(cookie: string) {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: cookie,
      },
    };
    await this.httpService.post('/Logout', {}, config);
  }
}
