import { Module } from '@nestjs/common';
import { LoginService } from './login.service';
import { HttpModule } from '../http/http.module';

@Module({
  imports: [HttpModule],
  providers: [LoginService],
  exports: [LoginService],
})
export class LoginModule {}
