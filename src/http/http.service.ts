import { Injectable, Inject, UnauthorizedException } from '@nestjs/common';
import Axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { Observable, of, defer, from } from 'rxjs';
import * as https from 'https';
import { map, catchError } from 'rxjs/operators';
import { REQUEST } from '@nestjs/core';
import { HttpServiceResponse } from './interface';

@Injectable()
export class HttpService {
  private axiosRef: AxiosInstance = Axios.create();
  private _session: string;

  constructor(@Inject(REQUEST) private request: any) {
    const requestInterceptor = (config: { headers: any }) => {
      this._session = config.headers.session;
      const avoidIntercept = ['Login'];
      let cookie = '';

      if (avoidIntercept.indexOf(config['url']) == -1) {
        if (this._session) {
          cookie = `B1SESSION=${this._session}`;
        } else {
          if (
            this.request &&
            this.request.headers &&
            this.request.headers.session
          ) {
            cookie = `B1SESSION=${this.request.headers.session}`;
          } else {
            if (
              this.request &&
              this.request.cookies &&
              this.request.cookies.session
            ) {
              cookie = `B1SESSION=${this.request.cookies.session}`;
            }
          }
        }
      }

      config.headers = {
        ...config.headers,
        'B1S-CaseInsensitive': true,
        Cookie: cookie,
      };

      this._session = null;
      return config;
    };
    this.axiosRef.defaults.baseURL = 'https://177.85.35.34:50000/b1s/v1';
    this.axiosRef.defaults.httpsAgent = new https.Agent({
      rejectUnauthorized: false,
    });
    this.axiosRef.interceptors.request.use(requestInterceptor);
  }

  responseHandler(r: AxiosResponse): HttpServiceResponse<any> {
    const result: HttpServiceResponse<any> = {
      error: null,
      data: r.data,
    };
    return result;
  }

  errorHandler(r: AxiosResponse): HttpServiceResponse<any> {
    if (r.status == 400 && r.data.error && r.data.error.message) {
      const result: HttpServiceResponse<any> = {
        error: {
          code: r.status.toString(),
          innerMessage: r.data.error.message.value.toString(),
        },
      };
      return result;
    } else {
      const result: HttpServiceResponse<any> = {
        error: {
          code: r.status.toString(),
          innerMessage: r.statusText,
        },
      };
      return result;
    }
  }

  catchErrorHandler(r: any): Observable<HttpServiceResponse<any>> {
    if (r.response) {
      if (
        r.response.status == 400 &&
        r.response.data.error &&
        r.response.data.error.message
      ) {
        const result: HttpServiceResponse<any> = {
          error: {
            code: r.response.status,
            innerMessage: r.response.data.error.message.value.toString(),
          },
        };
        return of(result);
      } else {
        const result: HttpServiceResponse<any> = {
          error: {
            code: r.response.status,
            innerMessage: r.response.statusText.toString(),
          },
        };
        return of(result);
      }
    } else {
      const result: HttpServiceResponse<any> = {
        error: {
          code: '500',
          innerMessage: r.toString(),
          message: 'Não foi possível conectar ao servidor. Tente novamente.',
        },
      };
      return of(result);
    }
  }

  session(value: string) {
    this._session = value;
    return this;
  }

  post(
    path: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Promise<HttpServiceResponse<any>> {
    return from(this.axiosRef.post(path, data, config))
      .pipe(
        map((r) => {
          if (r.status == 200 || r.status == 201 || r.status == 204) {
            return this.responseHandler(r);
          } else {
            return this.errorHandler(r);
          }
        }),
      )
      .pipe(catchError((r) => this.catchErrorHandler(r)))
      .toPromise();
  }

  get(path: string, config?: AxiosRequestConfig): Promise<any> {
    return from(this.axiosRef.get(path, config))
      .pipe(
        map((r) => {
          if (r.status == 200) {
            return this.responseHandler(r);
          } else {
            return this.errorHandler(r);
          }
        }),
      )
      .pipe(catchError((r) => this.catchErrorHandler(r)))
      .toPromise();
  }

  delete(path: string, config?: AxiosRequestConfig): Promise<any> {
    return from(this.axiosRef.delete(path, config))
      .pipe(
        map((r) => {
          if (r.status == 200) {
            return this.responseHandler(r);
          } else {
            return this.errorHandler(r);
          }
        }),
      )
      .pipe(catchError((r) => this.catchErrorHandler(r)))
      .toPromise();
  }

  put(
    path: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Promise<HttpServiceResponse<any>> {
    config = config || { headers: {} };
    config.headers['X-HTTP-Method-Override'] = 'PATCH';
    config.headers['B1S-ReplaceCollectionsOnPatch'] = 'true';
    return from(this.axiosRef.post(path, data, config))
      .pipe(
        map((r) => {
          if (r.status == 204) {
            return this.responseHandler(r);
          } else {
            return this.errorHandler(r);
          }
        }),
      )
      .pipe(
        catchError((r) => {
          return this.catchErrorHandler(r);
        }),
      )
      .toPromise();
  }

  patch(
    path: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Promise<HttpServiceResponse<any>> {
    config = config || { headers: {} };
    config.headers['X-HTTP-Method-Override'] = 'PATCH';
    config.headers['B1S-ReplaceCollectionsOnPatch'] = 'true';
    return from(this.axiosRef.patch(path, data, config))
      .pipe(
        map((r) => {
          if (r.status == 204) {
            return this.responseHandler(r);
          } else {
            return this.errorHandler(r);
          }
        }),
      )
      .pipe(
        catchError((r) => {
          return this.catchErrorHandler(r);
        }),
      )
      .toPromise();
  }

  putUpdate(
    path: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Promise<HttpServiceResponse<any>> {
    config = config || { headers: {} };
    config.headers['X-HTTP-Method-Override'] = 'PUT';
    config.headers['B1S-ReplaceCollectionsOnPatch'] = 'true';
    return from(this.axiosRef.post(path, data, config))
      .pipe(
        map((r) => {
          if (r.status == 204) {
            return this.responseHandler(r);
          } else {
            return this.errorHandler(r);
          }
        }),
      )
      .pipe(
        catchError((r) => {
          return this.catchErrorHandler(r);
        }),
      )
      .toPromise();
  }
}
