export class Item {
  ItemCode: string;
  ItemName: string;
  ItemType: string;
  ItemPrices: [
    {
      PriceList: number;
      Price: number;
    },
  ];
}
