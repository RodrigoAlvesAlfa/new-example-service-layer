import { Module } from '@nestjs/common';
import { LoginModule } from '../login/login.module';
import { ItemController } from './item.controller';
import { ItemService } from './item.service';
import { HttpModule } from '../http/http.module';

@Module({
  imports: [LoginModule, HttpModule],
  providers: [ItemService],
  controllers: [ItemController],
  exports: [ItemService],
})
export class ItemModule {}
