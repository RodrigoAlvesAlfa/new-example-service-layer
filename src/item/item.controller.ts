import {
  Controller,
  Post,
  Body,
  Get,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { Item } from './interface/item.interface';
import { ItemService } from './item.service';

@Controller('item')
export class ItemController {
  constructor(readonly itemService: ItemService) {}
  @Post()
  async createItem(@Body() body: Item) {
    return this.itemService.createItem(body);
  }

  @Get()
  async allItem() {
    return this.itemService.allItem();
  }

  @Patch()
  async updateItem(@Body() body: Item) {
    return this.itemService.updateItem(body);
  }

  @Get(':id')
  async getItemById(@Param() params) {
    return this.itemService.getItem(params.id);
  }

  @Delete(':id')
  async deleteItemById(@Param() params) {
    return this.itemService.deleteItem(params.id);
  }
}
