import { Injectable } from '@nestjs/common';
import { LoginService } from '../login/login.service';
import { Item } from './interface/item.interface';
import { HttpService } from '../http/http.service';

@Injectable()
export class ItemService {
  constructor(
    private readonly loginService: LoginService,
    private readonly httpService: HttpService,
  ) {}

  async createItem(data: Item) {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };

    const response = await this.httpService.post('Items', data, config);
    await this.loginService.logout(login.data.SessionId);
    if (response.data) {
      return {
        error: null,
        message: 'Create Item Success',
        code: response.data.ItemCode,
      };
    } else {
      return {
        error: response.error.innerMessage,
        message: 'Error to create Item',
        code: data.ItemCode,
      };
    }
  }

  async allItem() {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };
    const response = await this.httpService.get('/Items', config);

    await this.loginService.logout(login.data.SessionId);
    return response.data;
  }

  async updateItem(data: Item) {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };

    const item: any = await this.getItem(data.ItemCode);
    if (item.error) {
      return item;
    }
    const response = await this.httpService.patch(
      `/Items('${data.ItemCode}')`,
      data,
      config,
    );
    if (response.error) {
      return {
        error: response.error.innerMessage,
        message: 'Error to update item',
        code: data.ItemCode,
      };
    }

    await this.loginService.logout(login.data.SessionId);

    return {
      error: null,
      message: ' Update item success',
      code: data.ItemCode,
    };
  }

  async getItem(id: string) {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };
    const response = await this.httpService.get(`/Items('${id}')`, config);
    await this.loginService.logout(login.data.SessionId);
    if (!response.data) {
      return {
        error: response.error.innerMessage,
        message: 'Item not found',
        code: id,
      };
    }

    return response.data;
  }

  async deleteItem(id: string) {
    const login = await this.loginService.login();
    const config = {
      headers: {
        'Content-Type': 'application/json',
        session: login.data.SessionId,
      },
    };

    const item = await this.httpService.get(`/Items('${id}')`, config);
    if (!item.data) {
      return {
        error: item.error.innerMessage,
        message: 'Item not found',
        code: id,
      };
    }
    const response = await this.httpService.delete(`/Items('${id}')`, config);
    await this.loginService.logout(login.data.SessionId);
    if (response.error) {
      return {
        error: null,
        message: 'Item excluded',
        code: id,
      };
    } else {
      return {
        error: item.error.innerMessage,
        message: 'Item not excluded',
        code: id,
      };
    }
  }
}
